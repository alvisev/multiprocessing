"""
A small (concurrent.futures) multiprocessing test script for 'CPU bound'
problems.

As a toy example, this script parallelises linear regression on the penguins
dataset.
"""

import os
import time
import logging
import itertools
import concurrent.futures
import numpy as np
import seaborn as sns
from typing import Tuple

# Load penguins data (none of that iris / Fischer crap!)
penguins = sns.load_dataset('penguins')
penguins = penguins.dropna().reset_index(drop=True)
XX = penguins.drop(columns=['species', 'island', 'sex'])
YY = penguins.drop(columns=['species', 'island', 'sex'])


def process(args: Tuple[str, str]) -> np.ndarray:
    """Calculates linear regression coefficients :math:`\\hat{\\beta}`
    for linear regression model:

    .. math::

        y = \\beta_0 + \\beta_1 x + \\epsilon

    where :math:`\\hat{\\beta} \\equiv \\left(\\beta_0, \\beta_1 \\right)`.

    Args:
        args: ``DATA`` x and y labels

    Returns:
        linear fit coefficients :math:`\\hat{\\beta}`

    References:
        https://online.stat.psu.edu/stat462/node/132/
        https://cmdlinetips.com/2020/03/linear-regression-using-matrix-multiplication-in-python-using-numpy/
    """

    logger.info("\tPID: %s %s", os.getpid(), args)

    # Unpack arguments
    label_x, label_y = args

    # Fetch data
    global XX, YY
    x, y = XX[label_x], YY[label_y]

    # Form matrix adding x^0 terms
    X = np.vstack((np.ones(len(x)), x)).T

    # Calc linear coefficients such that y = X.dot(beta_hat)
    beta_hat = np.linalg.inv(X.T.dot(X)).dot(X.T).dot(y)

    logger.info("\t\tPID: %s FINISHED", os.getpid())
    return beta_hat


if __name__ == "__main__":

    # Setup a basic logger
    logging.basicConfig(
        format='[%(asctime)s] %(levelname)-8s | %(message)s',
        datefmt='%D %H:%M:%S'
    )

    logger = logging.getLogger(__name__)
    logger.setLevel("DEBUG")

    # Take all possible combinations of data columns
    params = itertools.product(XX.columns, YY.columns)
    params = list(params)

    start = time.time()

    logger.info("MULTIPROCESSING:")

    with concurrent.futures.ProcessPoolExecutor(max_workers=2) as executor:
        results = executor.map(process, params)  # returns a generator
        results = tuple(results)

    end = time.time()
    logger.info("EXECUTION TIME: %.2f s\n", end-start)

    logger.info("RESULTS:")
    for input, output in zip(params, results):
        logger.info("\t%s -> %s", input, output)
